# Gemnasium Python analyzer support matrix

| Date | GitLab version | Analyzer version|
| --- | --- | --- |
| 2022-05-22 | 15.0 | [3.x](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/master/CHANGELOG.md#v300) |
| 2019-01-04 | 11.7 - 14.10 | [2.x](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/-/blob/master/CHANGELOG.md#v200) |
| 2018-11-30 | 11.6 | [1.x](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/-/blob/master/CHANGELOG.md#v100) |
